class Machine {
    constructor() {
        this.isTurn = false;
    }

    turnOn(){
        this.isTurn = true;
        console.log('Вы включили ');
    }

    turnOff() {
        this.isTurn = false;
        console.log('Вы выключили ');
    }
}

class HomeAppliance extends Machine{
    constructor() {
        super();
        this.isPlug = false;
    }

    plugIn() {
        this.isPlug = true;
        console.log('Устройство включено в сеть')
    };
    plugOff() {
        this.isPlug = false;
        console.log('Устройство выключено из сеть!')
    }
}

class WashingMachine extends HomeAppliance {

    run() {
        if(this.isPlug && this.isTurn) {
            console.log('Стиралка включилась')
        } else {
            console.log('Сначала поключите к сети и включите стиралку!')
        }
    };
}

class LightSource extends HomeAppliance {
    constructor() {
        super();
        this.lightLevel = 0;
    }


    setLevel(level) {
        if(level > 1 && level < 100) {
            this.lightLevel = level;
            console.log('уровень освещенности ' + level);
        } else if (level < 1 || level > 100) {
            alert('Такого уровня нет');
        }
    }
}

class AutoVehicle extends Machine {
    constructor() {
        super();
        this.x = 0;
        this.y = 0;
    };
}

class car extends AutoVehicle {
    constructor() {
        super();
        this.setSpeed = 10;
    }
}